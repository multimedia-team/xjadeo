Source: xjadeo
Section: video
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 Alessio Treglia <alessio@debian.org>,
 Jaromír Mikeš <mira.mikes@seznam.cz>,
Build-Depends:
 debhelper-compat (= 13),
 libasound2-dev [linux-any],
 libavcodec-dev,
 libavformat-dev,
 libavutil-dev,
 libfreetype-dev,
 libglu1-mesa-dev | libglu-dev,
 libimlib2-dev,
 libjack-dev,
 liblo-dev,
 libltc-dev,
 libswscale-dev,
 libx11-dev,
 libxpm-dev,
 libxv-dev,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/multimedia-team/xjadeo.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/xjadeo
Homepage: http://xjadeo.sourceforge.net/

Package: xjadeo
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Suggests:
 mencoder,
Description: Video player with JACK sync
 Simple video player that receives sync from jackd or MTC.
 It has applications in soundtrack creation, video monitoring or any task that
 requires to associate movie frames with audio events.
 .
 For instance when a jack-client (like Muse, Rosegarden or Ardour) acts as a
 timebase master, xjadeo will display the video synchronized to JACK transport.
 xjadeo is capable to read Midi Time Clock as an alternate sync source.
 .
 xjadeo reads only seekable media by default. Installing a transcoding utility
 like mencoder or transcode is highly recommended.
 .
 On-line documentation:
 http://xjadeo.sourceforge.net/main.html
